<?php 
	session_start();
	if ($_SESSION["login"]){
		header('Location: index.php');
	}
	require 'config.php';

	$register_number = $_POST['register_number'];
	$password = $_POST['password'];

	// To protect from MySQL injection
	$register_number = stripslashes($register_number);
	$password = stripslashes($password);
	// $register_number = mysqli_real_escape_string($conn, $register_number);
	// $password = mysqli_real_escape_string($conn, $password);

	$query = "SELECT * FROM students where register_number = $register_number and password = '$password' limit 1";
	$result = mysqli_query($conn, $query);

	$row=mysqli_fetch_array($result,MYSQLI_ASSOC);

	if(mysqli_num_rows($result) == 1)
	{
		$_SESSION['register_number'] = $row['register_number']; // Initializing Session
		$_SESSION['name'] = $row['name']; // Initializing Session
		$_SESSION['email'] = $row['email']; // Initializing Session
		$_SESSION['student_id'] = $row['id']; // Initializing Session

		$student_id = $row['id'];
		$query = "SELECT * FROM marks where student_id = $student_id  limit 1";
		$result = mysqli_query($conn, $query);
		$mark_information=mysqli_fetch_array($result,MYSQLI_ASSOC);

		$_SESSION['x_mark'] = $mark_information['x_mark']; // Initializing Session
		$_SESSION['xii_mark'] = $mark_information['xii_mark']; // Initializing Session
		$_SESSION['cgpa'] = $mark_information['cgpa']; // Initializing Session
		$_SESSION['arrears_history'] = $mark_information['arrears_history']; // Initializing Session
		$_SESSION['arrears_standing'] = $mark_information['arrears_standing']; // Initializing Session

		$_SESSION['login'] = true; // Initializing Session
		header("location: index.php"); // Redirecting To Other Page
	}else
	{
		$error = "Incorrect username or password.";
		header("location: login.php"); 
	}

	echo $result;
?>