<?php 

session_start();
if (!$_SESSION["login"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Drive Information Page</title>
	<?php include "autoload.php" ?>
</head>
<body>
<?php
	$student_id = $_SESSION['student_id'];

	$x_mark = $_SESSION['x_mark'];
	$xii_mark = $_SESSION['xii_mark'];
	$cgpa = $_SESSION['cgpa'];
	$arrears_history = $_SESSION['arrears_history'];
	$arrears_standing = $_SESSION['arrears_standing'];

	$drive_id = $_GET['id'];
	$query = "SELECT * FROM drives where id = $drive_id limit 1 ";
	$result = mysqli_query($conn, $query);
	$drive=mysqli_fetch_array($result,MYSQLI_ASSOC);

	$query = "SELECT * FROM drive_students where student_id = $student_id and drive_id = $drive_id limit 1 ";
	$result = mysqli_query($conn, $query);
	$applied=mysqli_num_rows($result);

?>
<?php require 'partials/_header.php'; ?>

	<div class="container">
	<h1>Drive information</h1>
	<h3><?= $drive['company'] ?></h3>
	<p><?= $drive['description'] ?></p>
	<span class="label label-primary"><?= $drive['drive_date'] ?></span>
	<?php if ($applied): ?>
		<a href="#" class="btn btn-success pull-right" id="drive_apply" disabled>Applied </a>

	<?php elseif ($drive['minimum_x'] > $_SESSION['x_mark'] || $drive['minimum_xii'] > $_SESSION['xii_mark'] ||  $drive['minimum_cgpa'] > $_SESSION['cgpa'] || $drive['maximum_standing_arrears'] > $_SESSION['arrears_standing'] || $drive['maximum_history_arrears'] > $_SESSION['arrears_history']): ?>
		<a href="#" class="btn btn-danger pull-right"  disabled>Not Eligible</a>

	<?php else: ?>
		<a href="#" class="btn btn-info pull-right" id="drive_apply">Apply</a>

	<?php endif ?>
	<h3>Eligibility Criteria</h3>
	<table class="table table-bordered table-hover">
		<tbody>
			<tr>
				<td>Minimum 10<sup>th</sup></td>
				<td><?= $drive['minimum_x'] ?></td>
			</tr>
			<tr>
				<td>Minimum 12<sup>th</sup></td>
				<td><?= $drive['minimum_xii'] ?></td>
			</tr>
			<tr>
				<td>Minimum CGPA</td>
				<td><?= $drive['minimum_cgpa'] ?></td>
			</tr>
			<tr>
				<td>Maximum Standing Arrears Allowed</td>
				<td><?= $drive['maximum_standing_arrears'] ?></td>
			</tr>
			<tr>
				<td>Maximum History Of Arrears Allowed</td>
				<td><?= $drive['maximum_history_arrears'] ?></td>
			</tr>
		</tbody>
	</table>
	</div>
</body>

<script type="text/javascript">
	$("#drive_apply").bind('click',function(){
		$.ajax({
			url: "/drive_apply.php",
			data: {drive_id: "<?= $drive['id']?>", student_id: <?= $_SESSION['student_id'] ?> },
			method: "post",
			dataType: "json",
			success: function(data){
				if (data.success){
					$("#drive_apply").removeClass("btn-info").addClass('btn-success').attr('disabled',true).text('Applied');
				}
				else{
					alert(data.message)
				}
			}
		});

	});
</script>
</html>