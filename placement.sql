-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2016 at 11:22 PM
-- Server version: 5.6.25-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `placement`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `name`, `created_at`) VALUES
(1, 'admin', 'padmin', 'Mobin', '2016-02-01 07:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE IF NOT EXISTS `certificates` (
`id` int(11) NOT NULL,
  `certificate_name` varchar(10) NOT NULL,
  `student_id` int(11) NOT NULL,
  `certificate_path` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `certificate_name`, `student_id`, `certificate_path`, `created_at`) VALUES
(3, 'x_th', 1, '/certificates/1/x_th.jpg', '2016-03-31 15:35:21'),
(4, 'semester_1', 1, '/certificates/1/semester_1.jpg', '2016-03-31 15:36:19'),
(5, 'semester_6', 1, '/certificates/1/semester_6.png', '2016-03-31 15:36:34'),
(6, 'semester_8', 1, '/certificates/1/semester_8.jpg', '2016-03-31 16:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `drives`
--

CREATE TABLE IF NOT EXISTS `drives` (
`id` int(11) NOT NULL,
  `company` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `drive_date` date NOT NULL,
  `minimum_x` float NOT NULL,
  `minimum_xii` float NOT NULL,
  `minimum_cgpa` float NOT NULL,
  `maximum_standing_arrears` int(11) NOT NULL,
  `maximum_history_arrears` int(11) NOT NULL,
  `department` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drives`
--

INSERT INTO `drives` (`id`, `company`, `description`, `drive_date`, `minimum_x`, `minimum_xii`, `minimum_cgpa`, `maximum_standing_arrears`, `maximum_history_arrears`, `department`, `created_at`) VALUES
(3, 'Tech Mahendra', 'With Bootstrap 2, we added optional mobile friendly styles for key aspects of the framework. With Bootstrap 3, we''ve rewritten the project to be mobile friendly from the start. Instead of adding on optional mobile styles, they''re baked right into the core. In fact, Bootstrap is mobile first. Mobile first styles can be found throughout the entire library instead of in separate files.', '2016-02-04', 60, 60, 9.5, 0, 0, 'CSE', '2016-02-01 07:27:24'),
(10, 'sdf', 'sdfsdfsdf', '2016-02-11', 1, 1, 1, 1, 1, '', '2016-02-01 13:33:00');

-- --------------------------------------------------------

--
-- Table structure for table `drive_students`
--

CREATE TABLE IF NOT EXISTS `drive_students` (
`id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `drive_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drive_students`
--

INSERT INTO `drive_students` (`id`, `student_id`, `drive_id`, `created_at`) VALUES
(8, 1, 10, '2016-03-31 17:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE IF NOT EXISTS `marks` (
`id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `x_mark` float NOT NULL,
  `xii_mark` float NOT NULL,
  `semester_1` float NOT NULL,
  `semester_2` float NOT NULL,
  `semester_3` float NOT NULL,
  `semester_4` float NOT NULL,
  `semester_5` float NOT NULL,
  `semester_6` float NOT NULL,
  `semester_7` float NOT NULL,
  `semester_8` float NOT NULL,
  `cgpa` float NOT NULL,
  `arrears_history` int(11) NOT NULL,
  `arrears_standing` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `student_id`, `x_mark`, `xii_mark`, `semester_1`, `semester_2`, `semester_3`, `semester_4`, `semester_5`, `semester_6`, `semester_7`, `semester_8`, `cgpa`, `arrears_history`, `arrears_standing`, `approved`, `created_at`) VALUES
(2, 1, 80, 90, 5, 5, 5, 5, 5, 5, 5, 5, 8, 5, 1, 0, '2016-02-01 05:54:50'),
(3, 4, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-03-28 11:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `message` text NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `admin_id`, `created_at`) VALUES
(1, 'fgsdf', 1, '2016-03-31 15:52:29'),
(2, 'dfsdfdsf', 1, '2016-03-31 15:52:54'),
(3, 'sdadsd', 1, '2016-03-31 16:23:47'),
(4, 'sasswdsadsd', 1, '2016-03-31 17:35:21'),
(5, 'ddad', 1, '2016-03-31 17:45:22'),
(6, 'sdads\r\n\r\n', 1, '2016-03-31 17:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL DEFAULT 'password',
  `register_number` bigint(20) NOT NULL,
  `batch` varchar(10) NOT NULL,
  `department` varchar(25) NOT NULL,
  `section` varchar(1) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `password`, `register_number`, `batch`, `department`, `section`, `mobile`, `created_at`) VALUES
(1, 'Varun Raj', 'mailme@varunraj.in', 'varunraj', 111912104166, '2012-2016', 'CSE', 'C', 0, '2016-02-01 04:46:22'),
(5, 'AISHWARYALAKSHMI.G', 'aishwaryalakshmi42@yahoo.com', 'password', 111912104170, '2012-2016', 'CSE', 'C', 94442, '2016-02-01 12:35:58'),
(6, 'ANBARASAN.V.P', 'anbarasan.parthi@gmail.com', 'password', 111912104171, '2012-2016', 'CSE', 'C', 9942067566, '2016-02-01 12:35:58'),
(7, 'AROCKIA KIRIJAN.J', 'kirijan.kjv@gmail.com', 'password', 111912104172, '2012-2016', 'CSE', 'C', 9159779869, '2016-02-01 12:35:58'),
(8, 'ARULSELVI.K', 'VPr2124@gmail.com', 'password', 111912104173, '2012-2016', 'CSE', 'C', 9976865934, '2016-02-01 12:35:58'),
(9, 'ARUN KUMAR.P', 'arun04arun95@gmail.com', 'password', 111912104174, '2012-2016', 'CSE', 'C', 9841649305, '2016-02-01 12:35:58'),
(10, 'ARUN.A', 'arunk1899@gmail.com', 'password', 111912104175, '2012-2016', 'CSE', 'C', 8015902260, '2016-02-01 12:35:58'),
(11, 'ARUN.V', 'catherinesophia73@yahoo.in', 'password', 111912104176, '2012-2016', 'CSE', 'C', 9940611860, '2016-02-01 12:35:58'),
(12, 'ARUNKUMAR.T', 'aruntak2@gmail.com', 'password', 111912104177, '2012-2016', 'CSE', 'C', 9994252804, '2016-02-01 12:35:58'),
(13, 'ASHOK KUMAR.K', 'ashokshiff@gmail.com', 'password', 111912104178, '2012-2016', 'CSE', 'C', 9677002035, '2016-02-01 12:35:58'),
(14, 'ASHWIN.M', 'chanduvasasn@gmail.com', 'password', 111912104179, '2012-2016', 'CSE', 'C', 9500949088, '2016-02-01 12:35:58'),
(15, 'ASWINI.V', 'charmila.velu@gmail.com', 'password', 111912104180, '2012-2016', 'CSE', 'C', 9840061827, '2016-02-01 12:35:58'),
(16, 'BALAJI.B', 'nandhv_medical@yahoo.com', 'password', 111912104181, '2012-2016', 'CSE', 'C', 9444316099, '2016-02-01 12:35:58'),
(17, 'BALAJI.K', 'k_balaji94@ymail.com', 'password', 111912104182, '2012-2016', 'CSE', 'C', 9840998812, '2016-02-01 12:35:58'),
(18, 'BHARATH KUMAR.K', 'bhavathace10@gmail.com', 'password', 111912104183, '2012-2016', 'CSE', 'C', 9176808369, '2016-02-01 12:35:58'),
(19, 'BHARATHI.H', 'BharathiOF1994@gmail.com', 'password', 111912104184, '2012-2016', 'CSE', 'C', 9840107261, '2016-02-01 12:35:58'),
(20, 'BHARATHI.V', 'gnmonikasmee28@gmail.com', 'password', 111912104185, '2012-2016', 'ECE', 'C', 9840009078, '2016-02-01 12:35:58'),
(21, 'CHANDRAKUMAR.N', 'chandrakumar.stumer@gmail.com', 'password', 111912104186, '2013-2017', 'ECE', 'C', 9841443410, '2016-02-01 12:35:58'),
(22, 'DEEPAK.G', 'ashish.kumar7710@yahoo.co.in', 'password', 111912104187, '2013-2017', 'ECE', 'C', 9094640592, '2016-02-01 12:35:58'),
(23, 'DHANRAJ.R', 'atulgoswami94@gmail.com', 'password', 111912104188, '2013-2017', 'ECE', 'C', 9543120257, '2016-02-01 12:35:58'),
(24, 'DIMPLESHA.E', 'krish-sai26@yahoo.co.in', 'password', 111912104189, '2013-2017', 'ECE', 'C', 9840601912, '2016-02-01 12:35:58'),
(25, 'DINESHKUMAR.K', 'avinashy1993@gmail.com', 'password', 111912104190, '2013-2017', 'ECE', 'C', 9159917739, '2016-02-01 12:35:58'),
(26, 'DIVYA.P', 'Mohammedaynar94@gmail.com', 'password', 111912104191, '2013-2017', 'ECE', 'C', 9380907379, '2016-02-01 12:35:58'),
(27, 'ELAKIYA.E', 'crushbalaji@gmail.com', 'password', 111912104192, '2013-2017', 'ECE', 'C', 9283627317, '2016-02-01 12:35:58'),
(28, 'GAYATHRI.M', '', 'password', 111912104193, '2013-2017', 'ECE', 'C', 9884275423, '2016-02-01 12:35:58'),
(29, 'GEETHA.M', 'geethamelina@yahoo.com', 'password', 111912104194, '2013-2017', 'ECE', 'C', 9940109419, '2016-02-01 12:35:58'),
(30, 'GIRIDHARAN.P', 'BharathPrasadBalu@gmail.com', 'password', 111912104195, '2013-2017', 'ECE', 'C', 9042076481, '2016-02-01 12:35:58'),
(31, 'GOKUL RAJ.D', 'gocoolraj996@gmail.com', 'password', 111912104196, '2013-2017', 'ECE', 'C', 9962712636, '2016-02-01 12:35:58'),
(32, 'GOUTHAR FATHIMA.S.A.', 'sakthiragu143@gmail.com', 'password', 111912104197, '2013-2017', 'ECE', 'C', 9566187013, '2016-02-01 12:35:58'),
(33, 'GOWTHAMAN.C', 'Bibinthomas94@gmail.com', 'password', 111912104198, '2013-2017', 'ECE', 'C', 9941643109, '2016-02-01 12:35:58'),
(34, 'INBAVANAN.E', 'chidambaramkr94@gmail.com', 'password', 111912104199, '2013-2017', 'ECE', 'C', 9381816359, '2016-02-01 12:35:58'),
(35, 'JANARTHANAN.S', 'S.janarthanan13@gmail.com', 'password', 111912104200, '2013-2017', 'ECE', 'C', 9710465015, '2016-02-01 12:35:58'),
(36, 'JAYASHREE.P', 'deenapathy@yahoo.in', 'password', 111912104201, '2013-2017', 'ECE', 'C', 9941704396, '2016-02-01 12:35:58'),
(37, 'JAYASRINATH.J', 'Jagan591995@gmail.com', 'password', 111912104202, '2013-2017', 'ECE', 'C', 9003234442, '2016-02-01 12:35:58'),
(38, 'JEEVAN KUMAR.A', 'jaisongabriel2985@gmail.com', 'password', 111912104203, '2013-2017', 'ECE', 'C', 9841426699, '2016-02-01 12:35:58'),
(39, 'JENIFAR.T', 'itsme_sandya1994@rediffmail.com', 'password', 111912104204, '2013-2017', 'ECE', 'C', 984169248, '2016-02-01 12:35:58'),
(40, 'JENIFER.M', 'jefrinjoe@gmail.com', 'password', 111912104205, '2013-2017', 'ECE', 'C', 9444389539, '2016-02-01 12:35:58'),
(41, 'JOHIN RAJ.T', 'antonyjaikar@gmail.com', 'password', 111912104206, '2013-2017', 'ECE', 'C', 9840084029, '2016-02-01 12:35:58'),
(42, 'JOHNALEX.J', 'John.rockaj10@gmail.com', 'password', 111912104207, '2013-2017', 'ECE', 'C', 9043449208, '2016-02-01 12:35:58'),
(43, 'KALAISELVI.S', 'gkaarmy@ymail.com', 'password', 111912104208, '2013-2017', 'ECE', 'C', 9894789953, '2016-02-01 12:35:58'),
(44, 'KARTHICK.B', 'abishekapriiyanr1@gmail.com', 'password', 111912104209, '2013-2017', 'ECE', 'C', 9976991544, '2016-02-01 12:35:58'),
(45, 'KARTHIK.J', 'J.KARTHIK@OUTLOOK.COM', 'password', 111912104210, '2013-2017', 'ECE', 'C', 9380964052, '2016-02-01 12:35:58'),
(46, 'KARTHIK.M', 'anilgoswami15@gmail.com', 'password', 111912104211, '2013-2017', 'ECE', 'C', 9551330134, '2016-02-01 12:35:58'),
(47, 'KARTHIKEYAN.B', 'mkgeeeee@gmail.com', 'password', 111912104212, '2013-2017', 'ECE', 'C', 9500019944, '2016-02-01 12:35:58'),
(48, 'KAVIYARASAN.K', 'anjumaryabraham@rocketmail.com', 'password', 111912104213, '2013-2017', 'ECE', 'C', 9677147205, '2016-02-01 12:35:58'),
(49, 'KEERTHANA.D', 'kraj.1959@gmail.com', 'password', 111912104214, '2013-2017', 'ECE', 'C', 9444283666, '2016-02-01 12:35:58'),
(50, 'KEERTHIKA.V', 'saravananhbk123@gmail.com', 'password', 111912104215, '2013-2017', 'ECE', 'C', 9841086855, '2016-02-01 12:35:58'),
(51, 'KEERTHIVASAN.P.S', 'vasan.skeerthi@ymail.com', 'password', 111912104216, '2013-2017', 'ECE', 'C', 9444259117, '2016-02-01 12:35:58'),
(52, 'LAKSHMI.G', 'Ponnar3@gmail.com', 'password', 111912104217, '2013-2017', 'ECE', 'C', 9500615764, '2016-02-01 12:35:58'),
(53, 'LAVANYA.B', 'Lavanyabalu12@gmail.com', 'password', 111912104218, '2013-2017', 'ECE', 'C', 9444472632, '2016-02-01 12:35:58'),
(54, 'LOKESH.S', 'PoojaManohar11795@gmail.com', 'password', 111912104219, '2013-2017', 'ECE', 'C', 9176395380, '2016-02-01 12:35:58'),
(55, 'MADHAN.K.S.P', 'Madhanvijaya1995@gmail.com', 'password', 111912104220, '2013-2017', 'ECE', 'C', 9486045594, '2016-02-01 12:35:58'),
(56, 'MADHIVANAN.R', 'vlgarul@yahoo.com', 'password', 111912104221, '2013-2017', 'ECE', 'C', 9841175591, '2016-02-01 12:35:58'),
(57, 'MANIBHARATHI.S', 'Sjbharathi@gmail.com', 'password', 111912104222, '2013-2017', 'ECE', 'C', 9597237886, '2016-02-01 12:35:58'),
(58, 'MANIKANDAN.K', 'rahul13001@ovi.com', 'password', 111912104223, '2013-2017', 'ECE', 'C', 9710322473, '2016-02-01 12:35:58'),
(59, 'MANOGARAN.D', 'samueljoseph0420@gmail.com', 'password', 111912104224, '2013-2017', 'ECE', 'C', 8608544755, '2016-02-01 12:35:58'),
(60, 'MANOJ KUMAR.A', 'amk1994@rediffmail.com', 'password', 111912104225, '2013-2017', 'ECE', 'C', 9281396101, '2016-02-01 12:35:58'),
(61, 'MANOJ KUMAR.M', 'manojkumarbest.5@gmail.com', 'password', 111912104226, '2013-2017', 'ECE', 'C', 8122226116, '2016-02-01 12:35:58'),
(62, 'MEGHA.R', 'meghar33@gmail.com', 'password', 111912104227, '2013-2017', 'ECE', 'C', 9600067010, '2016-02-01 12:35:58'),
(66, 'EEE B', 'sandisrinivasan289@gmail.com', 'password', 111912104231, '2014-2018', 'ECE', 'C', 9281396101, '2016-02-01 12:35:58'),
(67, 'NAME', 'EMAIL ID', 'password', 111912104232, '2014-2018', 'ECE', 'C', 9281396101, '2016-02-01 12:35:58'),
(68, 'MONIKA.K.S', 'SanthoshKumar79@gmail.com', 'password', 111912104233, '2014-2018', 'ECE', 'C', 9543771881, '2016-02-01 12:35:58'),
(69, 'MURUGESH.S', 'swtkailash@yahoo.com', 'password', 111912104234, '2014-2018', 'ECE', 'C', 9710224160, '2016-02-01 12:35:58'),
(70, 'NANDA KUMAR.R', 'rrunitedraja@gmail.com', 'password', 111912104235, '2014-2018', 'ECE', 'C', 9380510563, '2016-02-01 12:35:58'),
(71, 'NARESH.S', 'Naresh2414@ymail.com', 'password', 111912104236, '2014-2018', 'MECH', 'C', 90947461169, '2016-02-01 12:35:58'),
(72, 'NIVEDITA.M', 'niveditamanoharan@rocketmail.com', 'password', 111912104237, '2014-2018', 'MECH', 'C', 9941271488, '2016-02-01 12:35:58'),
(73, 'NIVETHA.R', 'nivetharaja30@gmail.com', 'password', 111912104238, '2014-2018', 'MECH', 'C', 9444624407, '2016-02-01 12:35:58'),
(74, 'PADMAMALINI.T.H.', 'padmamalini94@gmailcom', 'password', 111912104239, '2014-2018', 'MECH', 'C', 9994626622, '2016-02-01 12:35:58'),
(75, 'PAVITHRA.G', 'smselva03@gmail.com', 'password', 111912104240, '2014-2018', 'MECH', 'C', 9790780364, '2016-02-01 12:35:58'),
(76, 'PRABHU.S', 'Prabhus489@gmail.com', 'password', 111912104241, '2014-2018', 'MECH', 'C', 9710253911, '2016-02-01 12:35:58'),
(77, 'PRADEEP.S', 'kpradeep277@gmail.com', 'password', 111912104242, '2014-2018', 'MECH', 'C', 8148335472, '2016-02-01 12:35:58'),
(78, 'PRADEEP.V', 'Pradeepv1994@gmail.com', 'password', 111912104243, '2014-2018', 'MECH', 'C', 9841977047, '2016-02-01 12:35:58'),
(79, 'PREETHI BANU.V', 'Preethibanu65@yahoo.com', 'password', 111912104244, '2014-2018', 'MECH', 'C', 8015965579, '2016-02-01 12:35:58'),
(80, 'PRIYA.B', 'sivasuryao@gmail.com', 'password', 111912104245, '2014-2018', 'MECH', 'C', 9940464688, '2016-02-01 12:35:58'),
(81, 'PRIYA.P', 'Sushmi-4@yahoo.co.in', 'password', 111912104246, '2014-2018', 'MECH', 'C', 9791957516, '2016-02-01 12:35:58'),
(82, 'PURNIMA.E', 'elamuruganicf@gmail.com', 'password', 111912104247, '2014-2018', 'MECH', 'C', 9003149157, '2016-02-01 12:35:58'),
(83, 'RAGAVENDRAN.C', 'ragavendran95@gmail.com', 'password', 111912104248, '2014-2018', 'MECH', 'C', 9962176063, '2016-02-01 12:35:58'),
(84, 'RAJA RAJESWARI.M', 'vaishali.rkp15595@yahoo.com', 'password', 111912104249, '2014-2018', 'MECH', 'C', 9444215923, '2016-02-01 12:35:58'),
(85, 'RAJESH KUMAR.D', 'devi846905@gmail.com', 'password', 111912104250, '2014-2018', 'MECH', 'C', 98659921202, '2016-02-01 12:35:58'),
(86, 'RAJESH.G', 'varsha1994@gmail.com', 'password', 111912104251, '2014-2018', 'MECH', 'C', 9790973094, '2016-02-01 12:35:58'),
(87, 'RAJESWARI.E', 'Vijay230594@gmail.com', 'password', 111912104252, '2014-2018', 'MECH', 'C', 9094396945, '2016-02-01 12:35:58'),
(88, 'RAMYA.S', 'vidhyagreddy@gmail.com', 'password', 111912104253, '2014-2018', 'MECH', 'C', 7373446267, '2016-02-01 12:35:58'),
(89, 'RAMYADEVI.S', 'rbpavislm@gmail.com', 'password', 111912104254, '2014-2018', 'MECH', 'C', 9940105386, '2016-02-01 12:35:58'),
(90, 'RANJITH KUMAR.K.G', 'jega.bharu@gmail.com', 'password', 111912104255, '2014-2018', 'MECH', 'C', 9573302829, '2016-02-01 12:35:58'),
(91, 'RANJITH KUMAR.M', 'sarathsundars@gmail.com', 'password', 111912104256, '2014-2018', 'MECH', 'C', 9840363890, '2016-02-01 12:35:58'),
(92, 'REVATHY.B.R.', 'janani2106@gmail.com', 'password', 111912104257, '2014-2018', 'MECH', 'C', 9840664269, '2016-02-01 12:35:58'),
(93, 'SAMBHAV.R', 'rangarsam@gmail.com', 'password', 111912104258, '2014-2018', 'MECH', 'C', 9382101346, '2016-02-01 12:35:58'),
(94, 'SANJEEV SELVAMANI.L.R.', 'Sanjeev95mailbox@rediff.com', 'password', 111912104259, '2014-2018', 'MECH', 'C', 9444871315, '2016-02-01 12:35:58'),
(95, 'SANKARAN.A', 'sarathsundars@gmail.com', 'password', 111912104260, '2014-2018', 'EEE', 'C', 9789073903, '2016-02-01 12:35:58'),
(96, 'SATHISHKUMAR.K', 'Sakx196@gmail.com', 'password', 111912104261, '2014-2018', 'IT', 'C', 9952027057, '2016-02-01 12:35:58'),
(97, 'SELVA KUMAR.S', 'smartsiv73@gmail.com', 'password', 111912104262, '2014-2018', 'IT', 'C', 9003122206, '2016-02-01 12:35:58'),
(98, 'SENTHIL KUMAR.R', 'susankumar@gmail.com', 'password', 111912104263, '2014-2018', 'IT', 'C', 9841303679, '2016-02-01 12:35:58'),
(99, 'SHAHANA FAREEN.S', 'harisrikal@ymail.com', 'password', 111912104264, '2014-2018', 'IT', 'C', 9710037234, '2016-02-01 12:35:58'),
(100, 'SHREE RAJESH RAAGUL.V', 'shreerajgul@gmail.com', 'password', 111912104265, '2014-2018', 'IT', 'C', 9566206913, '2016-02-01 12:35:58'),
(101, 'SIVAKUMAR.J', 'priyankargp479@gmail.com', 'password', 111912104266, '2014-2018', 'IT', 'C', 9940685458, '2016-02-01 12:35:58'),
(102, 'SRI HARI.B', 'alliswellayyappan@gmail.com', 'password', 111912104267, '2014-2018', 'IT', 'C', 9940564456, '2016-02-01 12:35:58'),
(103, 'SRINATH.S', 'srinath.shnmugam@yahoo.com', 'password', 111912104268, '2014-2018', 'IT', 'C', 9940498854, '2016-02-01 12:35:58'),
(104, 'STEPHEN JEBAKUMAR.J', 'john.Mech48@gmail.com', 'password', 111912104269, '2014-2018', 'EEE', 'C', 9710783240, '2016-02-01 12:35:58'),
(105, 'SUNDARESAN.M', 'alliswellayyappan@gmail.com', 'password', 111912104270, '2014-2018', 'EEE', 'C', 0, '2016-02-01 12:35:58'),
(106, 'SUNGANYA.A', 'vaish306@gmail.com', 'password', 111912104271, '2014-2018', 'EEE', 'C', 8508535793, '2016-02-01 12:35:58'),
(107, 'SWATHI.S', 'keerthanasathya89@gmail.com', 'password', 111912104272, '2014-2018', 'EEE', 'C', 9941007181, '2016-02-01 12:35:58'),
(108, 'TAMIL SELVAN.N', 'djtamil50@gmail.com', 'password', 111912104273, '2014-2018', 'EEE', 'C', 8015436693, '2016-02-01 12:35:58'),
(109, 'TAMIL SELVI.B', 'ptselvan965@gmail.com', 'password', 111912104274, '2014-2018', 'EEE', 'C', 9940492864, '2016-02-01 12:35:58'),
(110, 'THAMIM ANSARI.J', 'pankaj.sogain@gmail.com', 'password', 111912104275, '2014-2018', 'EEE', 'C', 8056055305, '2016-02-01 12:35:58'),
(111, 'THAMIZHARASAN.T', 'bhushandivyanshu@yahoo.in', 'password', 111912104276, '2014-2018', 'EEE', 'C', 7639145859, '2016-02-01 12:35:58'),
(112, 'THANGAMUNESWARAN.R', 'Thangamuneswaran143@gmail.com', 'password', 111912104277, '2014-2018', 'EEE', 'C', 7401175124, '2016-02-01 12:35:58'),
(113, 'THANGAPANDIAN .T', 'vaishnavimoorthy11@gmail.com', 'password', 111912104278, '2014-2018', 'EEE', 'C', 9566030757, '2016-02-01 12:35:58'),
(114, 'THIAGARAJAN.G', 'thiagarajang35@gmail.com', 'password', 111912104279, '2014-2018', 'EEE', 'C', 9566209097, '2016-02-01 12:35:58'),
(115, 'THULASI.G', 'thulasigtr7@gmail.com', 'password', 111912104280, '2014-2018', 'EEE', 'C', 9592436412, '2016-02-01 12:35:58'),
(116, 'VAISHALI.D', 'kumarashwin1996@gmail.com', 'password', 111912104281, '2014-2018', 'EEE', 'C', 9941062201, '2016-02-01 12:35:58'),
(117, 'VIDHYA.G', 'dhanacekiot@gmail.com', 'password', 111912104282, '2014-2018', 'EEE', 'C', 9381174196, '2016-02-01 12:35:58'),
(118, 'VIDHYA.K', 'VIDHYAKUMARKURUPARAN@gmail.com', 'password', 111912104283, '2014-2018', 'EEE', 'C', 9487139241, '2016-02-01 12:35:58'),
(119, 'VIGNESH.N', 'vignesh.nagarajan85@gmail.com', 'password', 111912104284, '2014-2018', 'EEE', 'C', 9962166704, '2016-02-01 12:35:58'),
(120, 'VIGNESH.S', 'aravindh051@gmail.com', 'password', 111912104285, '2014-2018', 'EEE', 'C', 9940662290, '2016-02-01 12:35:58'),
(121, 'VIJAY.T', 'Vijary_11847@ymail.com', 'password', 111912104286, '2014-2018', 'EEE', 'C', 8428376951, '2016-02-01 12:35:58'),
(123, 'saasasasa', 'varun.raj@skcript.com', 'password', 111912104166, '-4', '', '', 9600145622, '2016-03-31 16:36:03'),
(124, 'fgdsfdf', 'photonkingoo7@gmail.com', 'password', 166565, '-4', 'CSE', 'A', 6565, '2016-03-31 16:37:56'),
(125, 'dasdasd', 'photonkingoo7@gmail.com', 'password', 111912104166, '2012-2016', 'CSE', 'A', 155, '2016-03-31 16:38:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drives`
--
ALTER TABLE `drives`
 ADD PRIMARY KEY (`id`), ADD KEY `company` (`company`);

--
-- Indexes for table `drive_students`
--
ALTER TABLE `drive_students`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `register_number` (`register_number`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `drives`
--
ALTER TABLE `drives`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `drive_students`
--
ALTER TABLE `drive_students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=126;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
