<!DOCTYPE html>
<html>
<head>
	<title>Update Your Marks</title>
	<?php require 'autoload.php'; ?>
</head>
<body>
<?php require 'partials/_header.php';
$student_id = $_SESSION['student_id'];

$query = "SELECT * FROM students where id = $student_id  limit 1";
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

?>

<div class="container">
	<h3>Update Your Profile</h3>
  <hr>
	<form method="post" action="update_profile_submit.php">
	<div class="row">
	  <div class="form-group col-md-12">
	    <label for="name">Name</label>
	    <input type="text" class="form-control" name="name" value="<?= $row['name'] ?>" placeholder="Name">
	  </div>
	  <div class="form-group col-md-12">
	    <label for="email">Email Address</label>
	    <input type="email" class="form-control" name="email" value="<?= $row['email'] ?>" placeholder="Email Address">
	  </div>
	  <div class="form-group col-md-12">
	    <label for="mobile">Mobile Number</label>
	    <input type="text" class="form-control" name="mobile" value="<?= $row['mobile'] ?>" placeholder="Mobile Number">
	  </div>
	  <div class="form-group">
		  <div class="checkbox col-md-12">
		    <label>
		      <input type="checkbox" requried> Above mark is true to my knowledge
		    </label>
		  </div>
	  </div>
	  <div class="form-group">
		  <div class="col-md-12">
		  	<button type="submit" class="btn btn-default">Submit</button>
		  </div>
	  </div>
	</div>
</form>

<h3>Update Your Password</h3>
<hr>

<form class="form" action="update_password_submit.php" method="post">
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Password" required="">
	</div>
	<div class="form-group">
		<input type="password" name="retype_password" class="form-control" placeholder="Retype Password" required="">
	</div>
	<div class="form-group">
			<button type="submit" class="btn btn-default">Change Password</button>
	</div>
</form>
</div>
</body>
</html>
