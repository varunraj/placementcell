<!DOCTYPE html>
<html>
<head>
	<title>Update Your Marks</title>
	<?php require 'autoload.php'; ?>
</head>
<body>
<?php require 'partials/_header.php';
$student_id = $_SESSION['student_id'];

$query = "SELECT * FROM students where id = $student_id  limit 1";
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_array($result,MYSQLI_ASSOC);


?>

<div class="container">
	<h3>Add Certificates</h3>
  <hr>
	<form method="post" action="add_certificate_submit.php" enctype="multipart/form-data">
	<div class="row">
	  <div class="form-group col-md-12">
	    <label for="name">Certificate</label>
      <select class="form-control" name="certificate_name">
        <option value="x_th">10th Marksheet</option>
        <option value="xI_th">10th Marksheet</option>
        <option value="semester_1">1st Semester MarkSheet</option>
        <option value="semester_2">2nd Semester MarkSheet</option>
        <option value="semester_3">3rd Semester MarkSheet</option>
        <option value="semester_4">4th Semester MarkSheet</option>
        <option value="semester_5">5th Semester MarkSheet</option>
        <option value="semester_6">6th Semester MarkSheet</option>
        <option value="semester_7">7th Semester MarkSheet</option>
        <option value="semester_8">8th Semester MarkSheet</option>
      </select>
	  </div>
	  <div class="form-group col-md-12">
	    <label for="certificate_file">Certificate File</label>
	    <input type="file" class="form-control" name="certificate_file" placeholder="File" accept="image/*">
	  </div>

	  <div class="form-group">
		  <div class="checkbox col-md-12">
		    <label>
		      <input type="checkbox" requried> Above mark is true to my knowledge
		    </label>
		  </div>
	  </div>
	  <div class="form-group">
		  <div class="col-md-12">
		  	<button type="submit" class="btn btn-default">Submit</button>
		  </div>
	  </div>
	</div>
</form>

<div class="row">
	<h3>My Ceritifcates</h3>

	<?php
	$query = "SELECT * FROM certificates where student_id = $student_id";
	$result = mysqli_query($conn, $query);

 	while($certificate =  mysqli_fetch_array($result)): ?>
		<div class="col-md-3 ">
			<h3><?= $certificate['certificate_name'] ?></h3>

			<a href="<?= $certificate['certificate_path'] ?>" class="thumbnail">
				<img src="<?=  $certificate['certificate_path'] ?>" alt="" />
			</a>
		</div>
	<?php endwhile ?>
</div>
</div>
</body>
</html>
