<!DOCTYPE html>
<html>
<head>
	<?php require 'autoload.php';

	$student_id = $_GET['student_id'];
	$query = "SELECT * FROM students where id = $student_id  limit 1";
	$result = mysqli_query($conn, $query);
	$basic_information=mysqli_fetch_array($result,MYSQLI_ASSOC);

	 ?>
	<title><?php echo $basic_information['name']; ?>'s Profile</title>
</head>
<body>
	<?php require '../partials/_admin_header.php';

	$query = "SELECT `x_mark`, `xii_mark`, `semester_1`, `semester_2`, `semester_3`, `semester_4`, `semester_5`, `semester_6`, `semester_7`, `semester_8`, `cgpa`, `arrears_history`, `arrears_standing` FROM marks where student_id = $student_id  limit 1";
	$result = mysqli_query($conn, $query);
	$mark_information=mysqli_fetch_array($result,MYSQLI_ASSOC);
	?>

	<div class="container">
		<h3><?php echo $basic_information['name']; ?>'s Profile
				<a href="delete_student.php?student_id=<?= $student_id ?>" class="btn btn-danger btn-xs pull-right" onclick="return confirm('Are you sure?')">Delete</a>
		<a href="/admin/update_marks.php?student_id=<?=  $basic_information['id'] ?>" class="btn btn-primary btn-sm">Update Mark</a>
		</h3>
		<h4>Basic Information</h4>
		<table class="table table-bordered table-hover">
	  		<tbody>
	  			<tr>
		  			<td><strong>Name</strong></td>
		  			<td><?php echo $basic_information['name']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Email</strong></td>
		  			<td><?php echo $basic_information['email']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Register Number</strong></td>
		  			<td><?php echo $basic_information['register_number']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Batch</strong></td>
		  			<td><?php echo $basic_information['batch']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Department / Section</strong></td>
		  			<td><?php echo $basic_information['department']; ?> - <?php echo $basic_information['section']; ?> </td>
	  			</tr>
	  		</tbody>
	  	</table>

	  	<h4>Mark Information</h4>
			<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<td>Examination</td>
					<td>Marks</td>
				</tr>
			</thead>
	  		<tbody>
	  			<?php foreach ($mark_information as $key => $value): ?>
	  				<tr>
			            <td><strong><?= ucfirst(str_replace('_', ' ', $key)); ?></strong></td>
			  			<td><?= $value ?></td>
		  			</tr>
		        <?php endforeach ?>

	  		</tbody>
	  	</table>

			<div class="row">
				<h3>Ceritifcates</h3>

				<?php
				$query = "SELECT * FROM certificates where student_id = $student_id";
				$result = mysqli_query($conn, $query);
				$certificate_count = mysqli_num_rows($result);
				if ($certificate_count > 0) {
			 	while($certificate =  mysqli_fetch_array($result)): ?>
					<div class="col-md-3 ">
						<h3><?= $certificate['certificate_name'] ?></h3>

						<a href="<?= $certificate['certificate_path'] ?>" class="thumbnail">
							<img src="<?=  $certificate['certificate_path'] ?>" alt="" />
						</a>
					</div>
				<?php endwhile; } else { ?>
					<blockquote>
						No Certificates Now
					</blockquote>

					<?php	} ?>
			</div>
	</div>



</body>
</html>
