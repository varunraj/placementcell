<?php

session_start();
if (!$_SESSION["login"] && !$_SESSION["admin"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome Admin to Placement Cell</title>
	<?php include "autoload.php" ?>
</head>
<body>
<?php require '../partials/_admin_header.php'; ?>

<div class="container">
<h1>Welcome <?= $_SESSION['name'] ?> !</h1>
<?php
	$query = "SELECT count(*) as total FROM students";
	$result = mysqli_query($conn,$query);
	$data=mysqli_fetch_assoc($result);
	$students_count =  $data['total'];

	$query = "SELECT count(*) as total FROM drives";
	$result = mysqli_query($conn,$query);
	$data=mysqli_fetch_assoc($result);
	$drives_count =  $data['total'];
?>
<div class="row">
		<div class="col-md-6 well">
			<h1><?= $students_count ?></h1>
			<a href="<?= $BASE_URL ?>admin/students.php">Total Student</a>
		</div>
		<div class="col-md-6 well">
			<h1><?= $drives_count ?></h1>
			<a href="<?= $BASE_URL ?>admin/drives.php">Total Drives</a>
		</div>
</div>
<h3>Create an broadcast message</h3>
<div class="row">
	<div class="col-md-12">
		<form class="form" action="message_submit.php" method="post">
			<div class="form-group">
				<textarea name="message" class="form-control" cols="40" placeholder="Your message to broadcast"></textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-success pull-right">
			</div>
		</form>
	</div>
</div>
<hr>
<div class="col-md-12">
	<h3>Last 10 Messages</h3>
	<hr>

	<?php
	$query = "SELECT admins.name, messages.message,messages.created_at FROM admins INNER JOIN messages ON admins.id = messages.admin_id ORDER BY messages.created_at DESC LIMIT 10";
	$result = mysqli_query($conn,$query);
	while($message =  mysqli_fetch_array($result)): ?>
		 <h3>
			 	<?= $message['name'] ?>
		</h3>
		 <p>
		 		<?= $message['message'] ?>
		 </p>
 <?php endwhile ?>

</div>
</div>
</body>
</html>
