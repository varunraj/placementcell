<?php

session_start();
if ($_SESSION["login"] && $_SESSION["admin"]){
	session_destroy();
	header('Location: login.php');
}
 ?>
