<?php

session_start();
if (!$_SESSION["login"] && !$_SESSION["admin"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome Admin to Placement Cell</title>
	<?php include "autoload.php" ?>
</head>
<body>
<?php
	$query = "SELECT * FROM students";
	$result = mysqli_query($conn, $query);
?>
<?php require '../partials/_admin_header.php'; ?>

	<div class="container">

		<h3>
			List of Students
			<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#NewStudentModal">New Student</button>
		</h3>
		<table class="table table-bordered table-hover" id="students_table">
			<thead>
				<tr>
					<td>Name</td>
					<td>Register Number</td>
					<td>Email</td>
					<td>Department/Section</td>
					<td>Mobile</td>
					<td>Batch</td>
					<td>Option</td>
				</tr>
			</thead>
			<tbody>
				<?php while($student =  mysqli_fetch_array($result)): ?>
					<tr>
						<td><a href="profile.php?student_id=<?= $student['id'] ?>"><?= $student['name'] ?></a></td>
						<td><?= $student['register_number'] ?></td>
						<td><?= $student['email'] ?></td>
						<td><?= $student['department'] ?>/<?= $student['section'] ?></td>
						<td><?= $student['mobile'] ?></td>
						<td><?= $student['batch'] ?></td>
						<td><a href="/admin/update_marks.php?student_id=<?=  $student['id'] ?>" class="btn btn-primary btn-sm">Update Mark</a></td>
					</tr>
				<?php endwhile ?>
			</tbody>
		</table>
	</div>

	<div class="modal fade" id="NewStudentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add a new student</h4>
				</div>
				<form action="add_student_submit.php" method="post">
				<div class="modal-body">
					<div class="form-group">
					<label for="company">Student Name</label>
					<input type="text" class="form-control" name="name" placeholder="Student Name" required>
			</div>
			<div class="form-group">
					<label for="company">Student Email</label>
					<input type="email" class="form-control" name="email" placeholder="Student Email" required>
			</div>
			<div class="form-group">
					<label for="company">Register Number</label>
					<input type="number" class="form-control" name="register_number" placeholder="Register Number" required>
			</div>
			<div class="form-group">
					<label for="company">Mobile Number</label>
					<input type="number" class="form-control" name="mobile" placeholder="Mobile Number" required>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
							<label for="company">Batch</label>
							<select class="form-control" name="batch">
								<option value="2012-2016">2012-2016</option>
								<option value="2013-2017">2013-2017</option>
								<option value="2014-2018">2014-2018</option>
								<option value="2015-2019">2015-2019</option>
								<option value="2016-2020">2016-2020</option>
							</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="company">Department</label>
							<select class="form-control" name="department">
								<option value="CSE">CSE</option>
								<option value="ECE">ECE</option>
								<option value="EEE">EEE</option>
								<option value="IT">IT</option>
								<option value="Mech">Mech</option>
								<option value="CIVIL">CIVIL</option>
							</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="company">Section</label>
							<select class="form-control" name="section">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
							</select>
					</div>
				</div>
			</div>

			<!-- <div class="form-group">
						<label for="company">Departments</label>
						<input type="text" class="form-control" name="department" placeholder="" required>
			</div> -->
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$("#students_table").DataTable();
</script>
