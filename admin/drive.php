<?php

session_start();
if (!$_SESSION["login"] && !$_SESSION["admin"]){
		header('Location: login.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Drive Information Page</title>
	<?php include "autoload.php" ?>
</head>
<body>
<?php

	$drive_id = $_GET['id'];
	$query = "SELECT * FROM drives where id = $drive_id limit 1 ";
	$result = mysqli_query($conn, $query);
	$drive=mysqli_fetch_array($result,MYSQLI_ASSOC);

?>

<?php
	$query = "SELECT students.*,marks.*, drive_students.id as applied FROM `students` inner join marks on students.id = marks.student_id left join drive_students on students.id = drive_students.student_id WHERE marks.x_mark >= ". $drive["minimum_x"] . " and  marks.xii_mark >= ". $drive["minimum_xii"] . " and  marks.cgpa >= ". $drive["minimum_cgpa"] . " and  marks.arrears_standing >= ". $drive["maximum_standing_arrears"] . " and  marks.arrears_history >= ". $drive["maximum_history_arrears"] . " and drive_students.drive_id = $drive_id";

	$result = mysqli_query($conn, $query);
	$eligible_students_count = mysqli_num_rows($result);

?>
<?php require '../partials/_admin_header.php'; ?>

	<div class="container">
	<h1>Drive information
	<a href="delete_drive.php?drive_id=<?= $drive_id ?>" class="btn btn-danger btn-xs pull-right" onclick="return confirm('Are you sure?')">Delete</a>
	</h1>
	<h3><?= $drive['company'] ?></h3>
	<p><?= $drive['description'] ?></p>
	<h4><span class="label label-primary"><?= $drive['drive_date'] ?></span></h4>

	<h3>Eligibility Criteria</h3>
	<table class="table table-bordered table-hover">
		<tbody>
			<tr>
				<td>Minimum 10<sup>th</sup></td>
				<td><?= $drive['minimum_x'] ?></td>
			</tr>
			<tr>
				<td>Minimum 12<sup>th</sup></td>
				<td><?= $drive['minimum_xii'] ?></td>
			</tr>
			<tr>
				<td>Minimum CGPA</td>
				<td><?= $drive['minimum_cgpa'] ?></td>
			</tr>
			<tr>
				<td>Maximum Standing Arrears Allowed</td>
				<td><?= $drive['maximum_standing_arrears'] ?></td>
			</tr>
			<tr>
				<td>Maximum History Of Arrears Allowed</td>
				<td><?= $drive['maximum_history_arrears'] ?></td>
			</tr>
		</tbody>
	</table>
	<h3>Eligible Students (<?= $eligible_students_count ?>) <button type="button" id="sendmail" class="btn btn-sm btn-info">Send Email</button></h3>
	<p id="mail_status" style="color:green"></p>
	<p id="mail_sent_count">

	</p>
	<table class="table table-bordered table-hover" id="students_table">
			<thead>
				<tr>
					<td>Name</td>
					<td>Register Number</td>
					<td>Email</td>
					<td>Department/Section</td>
					<td>Mobile</td>
					<td>Batch</td>
					<td>Application Status</td>
				</tr>
			</thead>
			<tbody>
				<?php while($student =  mysqli_fetch_array($result)): ?>
					<tr>
						<td><?= $student['name'] ?></td>
						<td><?= $student['register_number'] ?></td>
						<td><?= $student['email'] ?></td>
						<td><?= $student['department'] ?>/<?= $student['section'] ?></td>
						<td><?= $student['mobile'] ?></td>
						<td><?= $student['batch'] ?></td>
						<td><?= $student['applied'] ? "<span class='label label-success'>Yes</span>" : "<span class='label label-danger'>No</span>" ?></td>
					</tr>
				<?php endwhile ?>
			</tbody>
		</table>
	</div>
</body>

<script type="text/javascript">
	var students_table = $("#students_table").dataTable();
	$("#sendmail").click(function(e){
		var myArray = new Array();

		$("#students_table tbody tr td:nth-child(3)").each(function(i){
        myArray.push($(this).text());
    });
		$("#mail_status").html('Sending...');

		var count;
		for (i = 0; i < myArray.length; i++) {
			$("#mail_status").html('Sending to ' + myArray[i] );
			count = i + 1;
			$.ajax({
				url: "mail.php",
				data: {company: "<?= $drive['company']?>", mail_id: myArray[i] },
				method: "post",
				dataType: "json",
				success: function(data){
					if (data.success){
						$("#mail_status").html('Sent to ' + data.mail_id );
						$("#mail_sent_count").html('Sent to ' + count + " Students" );
					}
					else{
						alert(data.message)
					}
				}.bind(count)
			});
		}


	});
</script>
</html>
