<!DOCTYPE html>
<html>
<head>
	<title>Admin Login - Placement Cell</title>
	<?php include 'autoload.php';
    session_start();
    if ($_SESSION["login"] && $_SESSION["admin"]){
        header('Location: index.php');
    }
    ?>
</head>
<body>

<div class="col-md-4 col-md-offset-4">
    <h3>Admin's Login</h3>
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Sign In</h3></div>
            <div class="panel-body">
                <form role="form" method="post" action="login_submit.php">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Username" name="username" type="text" autofocus="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="password" type="password" value="">
                        </div>
                        
                        <!-- Change this to a button or input when using this as a form -->
                        <input type="submit" value="Login" class="btn btn-success">
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>