<?php

session_start();
if (!$_SESSION["login"] && !$_SESSION["admin"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Listing Drives to Placement Cell</title>
	<?php include "autoload.php" ?>

</head>
<body>
<?php
$query = "SELECT admins.name, messages.message,messages.created_at FROM admins INNER JOIN messages ON admins.id = messages.admin_id ORDER BY messages.created_at LIMIT 10";
	$result = mysqli_query($conn, $query);
	$count = mysqli_num_rows($result);
	$row_count = 1;
?>
<?php require '../partials/_admin_header.php'; ?>

	<div class="container">

		<h3>List of BroadCast Messages (<?= $count ?>) </h3>

			<?php while($message =  mysqli_fetch_array($result)): ?>
				<div class="col-md-12 well">
					<h3><?= $message['message'] ?> </h3>
					<p><?= $message['name'] ?></p>
          <span class="label label-info"><?= $message['created_at'] ?></span>

				</div>
			<?php endwhile;  ?>
		</div>
	</div>

	<!-- New Drive Modal -->

</body>
</html>

<script type="text/javascript">

	$("#students_table").DataTable();

</script>
