<!DOCTYPE html>
<html>
<head>
	<title>Update Your Marks</title>
	<?php require 'autoload.php'; ?>
</head>
<body>
<?php require '../partials/_admin_header.php';
$student_id = $_GET['student_id'];
$query = "SELECT * FROM marks where student_id = $student_id  limit 1";
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

$query = "SELECT * FROM students where id = $student_id  limit 1";
$result = mysqli_query($conn, $query);
$row_student = mysqli_fetch_array($result,MYSQLI_ASSOC);
?>

<div class="container">
	<h3>Update Your Marks for <?= $row_student['name'] ?></h3>
	<p>Once you update the marks, it has to be approved by the admin in order to reflect</p>
	<form method="post" action="update_marks_submit.php?student_id=<?= $student_id ?>">
	<div class="row">
	  <div class="form-group col-md-4">
	    <label for="xthmark">10th Mark</label>
	    <input type="number" class="form-control" name="x_mark" value="<?= $row['x_mark'] ?>" placeholder="10th Mark">
	  </div>
	   <div class="form-group col-md-4">
	    <label for="xiithmark">12th Mark</label>
	    <input type="number" class="form-control" name="xii_mark" value="<?= $row['xii_mark'] ?>" placeholder="12th Mark">
	  </div>
	   <div class="form-group col-md-4">
	    <label for="semester1">1st Semester GPA </label>
	    <input type="number" class="form-control" name="semester_1" value="<?= $row['semester_1'] ?>" placeholder="1st Semester GPA">
	  </div>
	   <div class="form-group col-md-4">
	    <label for="semester2">2nd Semester GPA </label>
	    <input type="number" class="form-control" name="semester_2" value="<?= $row['semester_2'] ?>" placeholder="2nd Semester GPA">
	  </div>
	   <div class="form-group col-md-4">
	    <label for="semester3">3rd Semester GPA </label>
	    <input type="number" class="form-control" name="semester_3" value="<?= $row['semester_3'] ?>" placeholder="3rd Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="semester4">4th Semester GPA </label>
	    <input type="number" class="form-control" name="semester_4" value="<?= $row['semester_4'] ?>" placeholder="4th Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="semester5">5th Semester GPA </label>
	    <input type="number" class="form-control" name="semester_5" value="<?= $row['semester_5'] ?>" placeholder="5th Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="semester6">6th Semester GPA </label>
	    <input type="number" class="form-control" name="semester_6" value="<?= $row['semester_6'] ?>" placeholder="6th Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="semester7">7th Semester GPA </label>
	    <input type="number" class="form-control" name="semester_7" value="<?= $row['semester_7'] ?>" placeholder="7th Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="semester8">8th Semester GPA </label>
	    <input type="number" class="form-control" name="semester_8" value="<?= $row['semester_8'] ?>" placeholder="8th Semester GPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="cgpa">CGPA </label>
	    <input type="number" class="form-control" name="cgpa" value="<?= $row['cgpa'] ?>" placeholder="CGPA">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="cgpa">Number of Standing Arrears </label>
	    <input type="number" class="form-control" name="arrears_standing" value="<?= $row['arrears_standing'] ?>" placeholder="Number of Standing Arrears">
	  </div>
	  <div class="form-group col-md-4">
	    <label for="cgpa">Number of Total Arrears </label>
	    <input type="number" class="form-control" name="arrears_history" value="<?= $row['arrears_history'] ?>" placeholder="Number of Standing Arrears">
	  </div>
	  <div class="form-group">
		  <div class="checkbox col-md-12">
		    <label>
		      <input type="checkbox" requried> Above mark is true to my knowledge
		    </label>
		  </div>
	  </div>
	  <div class="form-group">
		  <div class="col-md-12">
		  	<button type="submit" class="btn btn-default">Submit</button>
		  </div>
	  </div>
	</div>
</form>
<div class="row">
	<h3>Ceritifcates</h3>

	<?php
	$query = "SELECT * FROM certificates where student_id = $student_id";
	$result = mysqli_query($conn, $query);

	while($certificate =  mysqli_fetch_array($result)): ?>
		<div class="col-md-3 ">
			<h3><?= $certificate['certificate_name'] ?></h3>

			<a href="<?= $certificate['certificate_path'] ?>" class="thumbnail">
				<img src="<?=  $certificate['certificate_path'] ?>" alt="" />
			</a>
		</div>
	<?php endwhile ?>
</div>
</div>
</body>
</html>
