<?php 

session_start();
if (!$_SESSION["login"] && !$_SESSION["admin"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Listing Drives to Placement Cell</title>
	<?php include "autoload.php" ?>

</head>
<body>
<?php
	$query = "SELECT * FROM drives";
	$result = mysqli_query($conn, $query);
	$count = mysqli_num_rows($result);
	$row_count = 1;
?>
<?php require '../partials/_admin_header.php'; ?>

	<div class="container">

		<h3>List of Drives (<?= $count ?>) <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">New Drive</button></h3>

			<?php while($drive =  mysqli_fetch_array($result)): ?>
				<?php if ($row_count % 4 == 0): ?>
				<div class="row">
				<?php endif ?>
				<div class="col-md-3 well">
					<h3><?= $drive['company'] ?></h3>
					<p><?= $drive['description'] ?></p>
					<span class="label label-primary"><?= $drive['drive_date'] ?></span>
					<a href="/admin/drive.php?id=<?= $drive['id']?>" class="btn btn-xs btn-success pull-right">Open </a>
				</div>
				<?php if ($row_count % 4 == 0): ?>
				</div>
				<?php endif ?>
			<?php $row_count = $row_count + 1; endwhile;  ?>
		</div>
	</div>

	<!-- New Drive Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Create a new drive</h4>
	      </div>
	      <form action="drive_submit.php" method="post">
	      <div class="modal-body">
	       	<div class="form-group">
			    <label for="company">Company Name</label>
			    <input type="text" class="form-control" name="company" placeholder="Company Name" required>
			</div>
			<div class="form-group">
			    <label for="company">Drive Description</label>
			    <textarea class="form-control" id="description_area" name="description" placeholder="Company Description" required></textarea>
			</div>
			<div class="form-group">
			    <label for="company">Drive Date</label>
			    <input type="date" class="form-control" name="date" placeholder="Drive Date" required>
			</div>
			<h5>Eligibility</h5>
			<div class="row">
				<div class="form-group col-md-4">
				    <label for="company">10th</label>
				    <input type="number" class="form-control" name="minimum_x" placeholder="Minimum 10th" required>
				</div>
				<div class="form-group col-md-4">
				    <label for="company">12th</label>
				    <input type="number" class="form-control" name="minimum_xii" placeholder="Minimum 12th" required >
				</div>
				<div class="form-group col-md-4">
				    <label for="company">CGPA</label>
				    <input type="number" class="form-control" name="minimum_cgpa" placeholder="Minimum CGPA" required>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
				    <label for="company">Allowed Standing Arrears</label>
				    <input type="number" class="form-control" name="maximum_standing_arrears" placeholder="" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="company">Allowed History of Arrears</label>
				    <input type="number" class="form-control" name="maximum_history_arrears" placeholder="" required>
				</div>
			</div>
			<!-- <div class="form-group">
				    <label for="company">Departments</label>
				    <input type="text" class="form-control" name="department" placeholder="" required>
			</div> -->
	      </div>
	      <div class="modal-footer">
	        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
</body>
</html>

<script type="text/javascript">

	$("#students_table").DataTable();

</script>