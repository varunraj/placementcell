<!DOCTYPE html>
<html>
<head>
	<?php require 'autoload.php'; ?>
	<title><?php echo $_SESSION['name']; ?>'s Profile</title>
</head>
<body>
	<?php require 'partials/_header.php';
	$student_id = $_SESSION['student_id'];

	$query = "SELECT * FROM students where id = $student_id  limit 1";
	$result = mysqli_query($conn, $query);
	$basic_information=mysqli_fetch_array($result,MYSQLI_ASSOC);

	$query = "SELECT `x_mark`, `xii_mark`, `semester_1`, `semester_2`, `semester_3`, `semester_4`, `semester_5`, `semester_6`, `semester_7`, `semester_8`, `cgpa`, `arrears_history`, `arrears_standing` FROM marks where student_id = $student_id  limit 1";
	$result = mysqli_query($conn, $query);
	$mark_information=mysqli_fetch_array($result,MYSQLI_ASSOC);
	?>

	<div class="container">
		<h3><?php echo $_SESSION['name']; ?>'s Profile</h3>
		<h4>Basic Information</h4>
		<table class="table table-bordered table-hover">
	  		<tbody>
	  			<tr>
		  			<td><strong>Name</strong></td>
		  			<td><?php echo $basic_information['name']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Email</strong></td>
		  			<td><?php echo $basic_information['email']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Register Number</strong></td>
		  			<td><?php echo $basic_information['register_number']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Batch</strong></td>
		  			<td><?php echo $basic_information['batch']; ?></td>
	  			</tr>
	  			<tr>
		  			<td><strong>Department / Section</strong></td>
		  			<td><?php echo $basic_information['department']; ?> - <?php echo $basic_information['section']; ?> </td>
	  			</tr>
	  		</tbody>
	  	</table>

	  		<h4>Mark Information</h4>
			<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<td>Examination</td>
					<td>Marks</td>
				</tr>
			</thead>
	  		<tbody>
	  			<?php foreach ($mark_information as $key => $value): ?>
	  				<tr>
			            <td><strong><?= ucfirst(str_replace('_', ' ', $key)); ?></strong></td>
			  			<td><?= $value ?></td>
		  			</tr>
		      <?php endforeach ?>

	  		</tbody>
	  	</table>
	</div>

</body>
</html>
