<?php

session_start();
if (!$_SESSION["login"]){
	header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome to Placement Cell</title>
	<?php include "autoload.php" ?>
</head>
<body>
<?php


	$x_mark = $_SESSION['x_mark'];
	$xii_mark = $_SESSION['xii_mark'];
	$cgpa = $_SESSION['cgpa'];
	$arrears_history = $_SESSION['arrears_history'];
	$arrears_standing = $_SESSION['arrears_standing'];

	$query = "SELECT * FROM drives where minimum_x <= $x_mark and minimum_xii <= $xii_mark and minimum_cgpa <= $cgpa and maximum_standing_arrears <= $arrears_standing and maximum_history_arrears <= $arrears_history ";
	$result = mysqli_query($conn, $query);
	$count = mysqli_num_rows($result);
	// $students=mysqli_fetch_array($result,MYSQLI_ASSOC);
?>
<?php require 'partials/_header.php'; ?>

	<div class="container">
	<h1>Welcome to Placement Portal</h1>
	<div class="row">
		<div class="col-md-8">
			<h3>List of drives based on your eligibility</h3>

		<?php while($drive =  mysqli_fetch_array($result)): ?>
			<div class="col-md-12">
				<h3><?= $drive['company'] ?>
					<span class="label label-primary"><?= $drive['drive_date'] ?></span>
				</h3>
				<p><?= $drive['description'] ?></p>
				<a href="/drive.php?id=<?= $drive['id']?>" class="btn btn-xs btn-success pull-right">Open </a>
			</div>
		<?php endwhile ?>
	</div>

	<div class="col-md-4">
		<h3>Last 10 BroadCast</h3>
		<hr>

		<?php
		$query = "SELECT admins.name, messages.message FROM admins INNER JOIN messages ON admins.id = messages.admin_id LIMIT 10";
		$result = mysqli_query($conn,$query);
		while($message =  mysqli_fetch_array($result)): ?>
			 <h3><?= $message['name'] ?></h3>
			 <p>
			 	<?= $message['message'] ?>
			 </p>
	 <?php endwhile ?>
	</div>
	</div>

	</div>
</body>
</html>
